<?php

/**
 * @file
 * Contains \Drupal\nodejs_watchdog\Logger\Nodejs.
 */

namespace Drupal\nodejs_watchdog\Logger;

use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Datetime\Date;
use Drupal\Component\Utility\Unicode;
use Drupal\Component\Utility\String;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Template\Attribute;

use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;

use Drupal\Core\Ajax\BeforeCommand;

use Drupal\dblog\Controller\DbLogController;


/**
 * Logs events in the watchdog database table.
 */
class Nodejs implements LoggerInterface {
  use LoggerTrait;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * Constructs a DbLog object.
   *
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   */
  public function __construct(LogMessageParserInterface $parser) {
    $this->parser = $parser;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = array()) {
    $commands = array();

    $classes = DbLogController::getLogLevelClassMap();

    $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);

    // Message to translate with injected variables.
    if (!empty($message_placeholders)) {
      $message = $this->t($message, $message_placeholders);
    }
    if (isset($context['link'])) {
      // Truncate link_text to 56 chars of message.
      $log_text = Unicode::truncate(Xss::filter($message, array()), 56, TRUE, TRUE);
      $message = l($log_text, 'dblog.event',  array('event_id' => $context['link']), array('html' => TRUE));
    }

    $username = array(
      '#theme' => 'username',
      '#account' => user_load($context['uid']),
    );
    $rows[] = array(
      'data' => array(
        // Cells.
        array('class' => array('icon')),
        substr($context['channel'], 0, 64),
        \Drupal::service('date')->format($context['timestamp'], 'short'),
        $message,
        array('data' => $username),
        Xss::filter(substr($context['link'], 0, 255)),
      ),
      // Attributes for table row.
      'class' => array(drupal_html_class('dblog-' . substr($context['channel'], 0, 64)), $classes[$level]),
    );

    $build['dblog_table'] = array(
      '#type' => 'table',
      '#rows' => $rows,
      '#attributes' => array('id' => 'admin-dblog', 'class' => array('admin-dblog')),
    );

    // FIXME: This is a terrible quick hack!
    // It would be better to use a custom theme template for this. 
    $output = drupal_render($build);
    $loc = stripos($output, "<tr class");
    $output = substr("$output", $loc);
    $loc = stripos($output, "</tr>");
    $output = substr("$output", 0, $loc+5);

    // TODO: Get this custom template working
    // $build['row'] = array(
    //   '#theme' => 'nodejs_watchdog_row',
    //   '#row' => $row,
    // );
  
    $command = new BeforeCommand('#admin-dblog tr:eq(1)', $output);
    $commands[] = $command->render();

    $message = (object) array(
      'channel' => 'watchdog_dblog',
      'commands' => $commands,
      'callback' => 'nodejsWatchdog',
    );
    nodejs_send_content_channel_message($message);
  }

}
