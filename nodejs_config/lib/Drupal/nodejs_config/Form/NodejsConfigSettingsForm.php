<?php

/**
 * @file
 * Contains \Drupal\nodejs_config\Form\NodejsConfigSettingsForm.
 */

namespace Drupal\nodejs_config\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Component\Utility\String;

/**
 * Node.js server configuration form.
 */
class NodejsConfigSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nodejs_config_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $config = $this->config('nodejs_config.settings');
    $suggestion = $config->get('nodejs_config_js_suggestion');
    $file_path = drupal_get_path('module', 'nodejs') . '/nodejs.config.js';
    $config_path_message = $this->t('<ol><li>Configure your server settings in the SETTINGS form below.</li><li>Click the <b>Save Configuration</b> button.</li><li>Copy the suggested configuration lines to <b>!file</b>, you need to do it manually.</li></ol>', array('!file' => $file_path));

    $form['config_suggestion'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Configuration builder'),
      '#description' => $config_path_message,
    );
    $form['config_suggestion']['nodejs_config_js'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Suggested configuration:'),
      '#default_value' => $suggestion,
    );
    $form['config'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Settings'),
    );
    $form['config']['nodejs_config_host'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#required' => TRUE,
      '#description' => $this->t('Specify the host name or IP address that the node.js server should listen on.
                                  Leave blank to listen for any host name. Otherwise, the server will only respond
                                  to names that match the IP address given (or resolved from the given name).'),
      '#default_value' => $config->get('nodejs_config_host'),
    );
    $form['config']['nodejs_config_port'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Port'),
      '#required' => TRUE,
      '#description' => $this->t('Specify the TCP port that the node.js server should listen on.'),
      '#default_value' => $config->get('nodejs_config_port'),
    );

    $scheme = $config->get('nodejs_server_scheme');
    $form['config']['nodejs_config_key'] = array(
      '#type' => $scheme == 'https' ? 'textfield' : 'hidden',
      '#title' => $this->t('Key'),
      '#required' => TRUE,
      '#description' => $this->t('File system path to a key used for https communication with the server and clients.'),
      '#default_value' => $config->get('nodejs_config_key'),
    );
    $form['config']['nodejs_config_cert'] = array(
      '#type' => $scheme == 'https' ? 'textfield' : 'hidden',
      '#title' => $this->t('Cert'),
      '#required' => TRUE,
      '#description' => $this->t('File system path to a certificate used for https communication with the server and clients.'),
      '#default_value' => $config->get('nodejs_config_cert'),
    );
    $form['config']['nodejs_config_resource'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Resource'),
      '#required' => TRUE,
      '#description' => $this->t('http path that the node.js server should respond to.
                                  This value needs to match the Drupal node.js configuration.'),
      '#default_value' => $config->get('nodejs_config_resource'),
    );
    $form['config']['nodejs_config_publishUrl'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Publish URL'),
      '#required' => TRUE,
      '#description' => $this->t('http path on which the node.js server should accept messages from the Drupal site.'),
      '#default_value' => $config->get('nodejs_config_publishUrl'),
    );
    $form['config']['nodejs_service_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Service Key'),
      '#description' => $this->t('An arbitrary string used as a secret between the node.js server and the Drupal site.'),
      '#default_value' => $config->get('nodejs_service_key'),
    );
    $form['config']['nodejs_config_write_channels'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Client write to channels'),
      '#description' => $this->t('Global flag that allows all channels to be written to by client sockets without
                                  going via the backend. Defaults to false.'),
      '#default_value' => $config->get('nodejs_config_write_channels'),
    );
    $form['config']['nodejs_config_write_clients'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Client write to clients'),
      '#description' => $this->t('Global flag that allows all clients to be written to by client sockets
                                  without going via the backend. Defaults to false.'),
      '#default_value' => $config->get('nodejs_config_write_clients'),
    );
    $form['config']['backend'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Backend'),
    );
    $form['config']['backend']['nodejs_config_backend_host'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#required' => TRUE,
      '#description' => $this->t('Host name of the Drupal site.'),
      '#default_value' => $config->get('nodejs_config_backend_host'),
    );
    $form['config']['backend']['nodejs_config_backend_port'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Port'),
      '#required' => TRUE,
      '#description' => $this->t('TCP port of the server running the Drupal site. Usually 80.'),
      '#default_value' => $config->get('nodejs_config_backend_port'),
    );
    $form['config']['backend']['nodejs_config_backend_messagePath'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Auth Path'),
      '#description' => $this->t('http path on which the Drupal node.js module listens for authentication check
                                  requests. Must end with /.'),
      '#default_value' => $config->get('nodejs_config_backend_messagePath'),
    );
    $form['config']['backend']['nodejs_config_debug'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Debug'),
      '#description' => $this->t('Show debug information from the node.js process.'),
      '#default_value' => $config->get('nodejs_config_debug'),
    );
    $form['ext'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Extensions'),
      '#description' => $this->t('An array of names of node.js modules that should be loaded as extensions to the
                                  node.js server.'),
    );
    $form['ext']['nodejs_config_extensions'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Extension file paths'),
      '#description' => $this->t('A list of node.js extension files paths, one per line.'),
      '#default_value' => $config->get('nodejs_config_extensions'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $config = $this->configFactory->get('nodejs_config.settings');
    $values = $form_state['values'];
    $ext = $values['nodejs_config_extensions'];
    if ($ext == !NULL) {
      $ext = explode("\n", str_replace("\r", '', $ext));
    }
    $array = array(
      'scheme' => $config->get('nodejs_server_scheme'),
      'host' => String::checkPlain($values['nodejs_config_host']),
      'port' => (int) $values['nodejs_config_port'],
      'key' => String::checkPlain($values['nodejs_config_key']),
      'cert' => String::checkPlain($values['nodejs_config_cert']),
      'resource' => String::checkPlain($values['nodejs_config_resource']),
      'publishUrl' => String::checkPlain($values['nodejs_config_publishUrl']),
      'serviceKey' => String::checkPlain($values['nodejs_service_key']),
      'backend' => array(
        'port' => (int) $values['nodejs_config_backend_port'],
        'host' => String::checkPlain($values['nodejs_config_backend_host']),
        'messagePath' => String::checkPlain($values['nodejs_config_backend_messagePath']),
      ),
      'clientsCanWriteToChannels' => (bool) $values['nodejs_config_write_channels'],
      'clientsCanWriteToClients' => (bool) $values['nodejs_config_write_clients'],
      'extensions' => $ext,
      'debug' => (bool) $values['nodejs_config_debug'],
      'transports' => array(
        'websocket', 'flashsocket', 'htmlfile', 'xhr-polling', 'jsonp-polling'
      ),
      'jsMinification' => true,
      'jsEtag' => true,
      'logLevel' => 1
    );
    $output = 'backendSettings = ' . drupal_json_encode($array);
    $output = str_replace(array('= {', ',', '}}', ':{', '\/'), array("= {\n  ", ",\n  ", "\n  }\n}", ":{\n  ",  '/'), $output);
    $output = "/**\n* This configuration file was built using the 'Node.js server configuration builder'.\n* For a more fully commented example see the file nodejs.config.js.example in the root of this module\n*/\n" . $output . ';';

    $this->configFactory->get('nodejs_config.settings')
      ->set('nodejs_config_js_suggestion', $output)
      ->set('nodejs_config_host', String::checkPlain($values['nodejs_config_host']))
      ->set('nodejs_config_port', (int) $values['nodejs_config_port'])
      ->set('nodejs_config_key', String::checkPlain($values['nodejs_config_key']))
      ->set('nodejs_config_cert', String::checkPlain($values['nodejs_config_cert']))
      ->set('nodejs_config_resource', String::checkPlain($values['nodejs_config_resource']))
      ->set('nodejs_config_publishUrl', String::checkPlain($values['nodejs_config_publishUrl']))
      ->set('nodejs_service_key', String::checkPlain($values['nodejs_service_key']))
      ->set('nodejs_config_backend_port', (int) $values['nodejs_config_backend_port'])
      ->set('nodejs_config_backend_host', String::checkPlain($values['nodejs_config_backend_host']))
      ->set('nodejs_config_backend_messagePath', String::checkPlain($values['nodejs_config_backend_messagePath']))
      ->set('nodejs_config_write_channels', (bool) $values['nodejs_config_write_channels'])
      ->set('nodejs_config_write_clients', (bool) $values['nodejs_config_write_clients'])
      ->set('nodejs_config_debug', (bool) $values['nodejs_config_debug'])
      ->set('nodejs_config_extensions', $values['nodejs_config_extensions'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
