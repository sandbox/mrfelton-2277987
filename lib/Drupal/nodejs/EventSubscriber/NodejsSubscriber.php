<?php

/**
 * @file
 * Contains \Drupal\nodejs\EventSubscriber\NodejsSubscriber.
 */

namespace Drupal\nodejs\EventSubscriber;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class NodejsSubscriber implements EventSubscriberInterface {

  public function initLibraries(GetResponseEvent $event) {
    drupal_register_shutdown_function(array('Nodejs', 'sendMessages'));
    if (nodejs_add_js_to_page_check()) {
      $attached['#attached']['library'][] = 'nodejs/init';
      drupal_render($attached);
    }
    
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('initLibraries', 40);
    return $events;
  }

}
