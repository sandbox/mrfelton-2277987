<?php

/**
 * @file
 * Contains \Drupal\system\Form\NodejsConfigForm.
 */

namespace Drupal\nodejs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure node.js integration.
 */
class NodejsConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'nodejs_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $config = $this->configFactory->get('nodejs.config');
    $form['server'] = array(
      '#type' => 'fieldset',
      '#title' => t('Node.js server'),
    );
    $form['server']['nodejs_server_scheme'] = array(
      '#type' => 'radios',
      '#title' => t('Protocol used by Node.js server'),
      '#default_value' => $config->get('nodejs_server_scheme'),
      '#options' => array('http' => t('http'), 'https' => t('https')),
      '#description' => t('The protocol used to communicate with the Node.js server.'),
    );
    $form['server']['nodejs_server_host'] = array(
      '#type' => 'textfield',
      '#title' => t('Node.js server host'),
      '#default_value' => $config->get('nodejs_server_host'),
      '#size' => 40,
      '#required' => TRUE,
      '#description' => t('The hostname of the Node.js server.'),
    );
    $form['server']['nodejs_server_port'] = array(
      '#type' => 'textfield',
      '#title' => t('Node.js server port'),
      '#default_value' => $config->get('nodejs_server_port'),
      '#size' => 10,
      '#required' => TRUE,
      '#description' => t('The port of the Node.js server.'),
    );
    $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard.
                      Example paths are %blog for the blog page and %blog-wildcard for every personal blog.
                      %front is the front page.",
                      array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
    $form['server']['nodejs_pages'] = array(
      '#type' => 'textarea',
      '#title' => t('Pages on which to enable nodejs'),
      '#default_value' => $config->get('nodejs_pages'),
      '#required' => TRUE,
      '#description' => $description,
    );

    return parent::buildForm($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $this->configFactory->get('nodejs.config')
      ->set('nodejs_server_scheme', $form_state['values']['nodejs_server_scheme'])
      ->set('nodejs_server_host', $form_state['values']['nodejs_server_host'])
      ->set('nodejs_server_port', $form_state['values']['nodejs_server_port'])
      ->set('nodejs_pages', $form_state['values']['nodejs_pages'])
      ->save();

    parent::submitForm($form, $form_state);
  }
}
