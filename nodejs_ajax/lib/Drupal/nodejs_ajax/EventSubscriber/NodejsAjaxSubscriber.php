<?php

/**
 * @file
 * Contains \Drupal\nodejs_ajax\EventSubscriber\NodejsAjaxSubscriber.
 */

namespace Drupal\nodejs_ajax\EventSubscriber;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class NodejsAjaxSubscriber implements EventSubscriberInterface {

  public function initLibraries(GetResponseEvent $event) {
    $attached['#attached']['library'][] = 'core/drupal.ajax';
    drupal_render($attached);
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('initLibraries');
    return $events;
  }

}
